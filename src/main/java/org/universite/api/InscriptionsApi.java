package org.universite.api;

import org.universite.model.Inscription;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import org.apache.cxf.jaxrs.ext.multipart.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import io.swagger.jaxrs.PATCH;
import javax.validation.constraints.*;
import javax.validation.Valid;

/**
 * APIs de gestion des inscriptions
 *
 * <p>Cette API propose la gestion des inscriptions des étudiants
 *
 */
@Path("/")
@Api(value = "/", description = "")
public interface InscriptionsApi  {

    /**
     * postInscription
     *
     * Inscrit un nouvel étudiant
     *
     */
    @POST
    @Path("/inscriptions")
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/json", "application/xml" })
    @ApiOperation(value = "postInscription", tags={ "inscriptions",  })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created"),
        @ApiResponse(code = 400, message = "Echec de création, données invalides") })
    public void addInscriptions(@Valid Inscription body);

    /**
     * getAllInscriptions
     *
     * Fournit la liste des inscrits
     *
     */
    @GET
    @Path("/inscriptions")
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/json", "application/xml" })
    @ApiOperation(value = "getAllInscriptions", tags={ "inscriptions",  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Inscription.class, responseContainer = "List") })
    public List<Inscription> getAllInscriptions();

    /**
     * getInscriptions
     *
     * Fournit les informations sur une inscription
     *
     */
    @GET
    @Path("/inscriptions/{identifiant}")
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/json", "application/xml" })
    @ApiOperation(value = "getInscriptions", tags={ "inscriptions" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Inscription.class),
        @ApiResponse(code = 404, message = "Identificant étudiant inconnu") })
    public Inscription getInscriptions(@PathParam("identifiant") String identifiant);
}

