package org.universite.api;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.universite.api.InscriptionsApi;

import org.universite.model.Adresse;
import org.universite.model.Inscription;


@CrossOriginResourceSharing(allowAllOrigins = false)
@Component("InscriptionsApi")
public class InscriptionsApiServiceImpl implements InscriptionsApi {


    private static final Logger LOG = Logger.getLogger(InscriptionsApiServiceImpl.class.getName());
    
    static List<Inscription> listeInscrits = new ArrayList<Inscription>();

    static {
        listeInscrits.add(new Inscription().etudiantId("1234")
                .nom("SIMON")
                .prenom("Michel")
                .adresse(new Adresse().numeroVoie(12)
                        .typeVoie(Adresse.TypeVoieEnum.BOULEVARD)
                        .nomVoie("des hirondelles")
                        .codePostal("31000")
                        .commune("Toulouse")));
        listeInscrits.add(new Inscription().etudiantId("43267")
                .nom("GABIN")
                .prenom("Jean")
                .adresse(new Adresse().codePostal("75000")
                        .commune("Paris")));

    }

    @CrossOriginResourceSharing(allowOrigins = {"http://localhost:8090"})
    public List<Inscription> getAllInscriptions() {
        LOG.info("Appel recu sur getAllInscriptions");
        return listeInscrits;

        //new NotFoundException()
        //utiliser la classe Link pour la cr�ation de liens HATEOAS
        // builder starts with current URI and has the path of the getCustomer method appended to it
        /*  UriBuilder ub = ui.getAbsolutePathBuilder().path(this.getClass(), "getCustomer");
        for (Customer customer: customers) {
            // builder has {id} variable that must be filled in for each customer
            URI uri = ub.build(customer.getId());
            c.setUrl(uri);
        }
        return customers;*/
    }

    @Override
    public Inscription getInscriptions(String id)  {
        Inscription inscritTrouve = null;
        for (Inscription inscrit : listeInscrits) {
            if (inscrit.getEtudiantId().equals(id)) {
                inscritTrouve = inscrit;
            }
        }
        if (null != inscritTrouve) {
            return inscritTrouve;
        } else {
//              throw new Error();
            throw new WebApplicationException( "Impossible de retrouver cet identifiant : " + id,Response.status(Response.Status.NOT_FOUND).build());
        }
    }

    @Override
    public void addInscriptions(Inscription inscription) {
        listeInscrits.add(inscription);
    }

}
