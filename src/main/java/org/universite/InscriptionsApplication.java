package org.universite;

import java.util.Locale;
import org.apache.cxf.jaxrs.validation.ValidationExceptionMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@SpringBootApplication
public class InscriptionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InscriptionsApplication.class, args);
	}

@Bean
public LocaleResolver localeResolver() {
    SessionLocaleResolver slr = new SessionLocaleResolver();
    slr.setDefaultLocale(Locale.FRENCH);
    return slr;
} 


@Bean
ValidationExceptionMapper exceptionMapper() {
    ValidationExceptionMapper vem= new ValidationExceptionMapper();
    vem.setAddMessageToResponse(true);
    return vem;
}

}