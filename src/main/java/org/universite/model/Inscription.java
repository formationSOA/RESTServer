package org.universite.model;

import org.universite.model.Adresse;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Inscription  {
  
  @ApiModelProperty(required = true, value = "")
  private String etudiantId = null;

  @ApiModelProperty(required = true, value = "")
  private String nom = null;

  @ApiModelProperty(value = "")
  private String prenom = null;

  @ApiModelProperty(value = "")
  private Adresse adresse = null;
 /**
   * Get etudiantId
   * @return etudiantId
  **/
  @JsonProperty("etudiantId")
  @NotNull
  public String getEtudiantId() {
    return etudiantId;
  }

  public void setEtudiantId(String etudiantId) {
    this.etudiantId = etudiantId;
  }

  public Inscription etudiantId(String etudiantId) {
    this.etudiantId = etudiantId;
    return this;
  }

 /**
   * Get nom
   * @return nom
  **/
  @JsonProperty("nom")
  @NotNull
  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public Inscription nom(String nom) {
    this.nom = nom;
    return this;
  }

 /**
   * Get prenom
   * @return prenom
  **/
  @JsonProperty("prenom")
  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public Inscription prenom(String prenom) {
    this.prenom = prenom;
    return this;
  }

 /**
   * Get adresse
   * @return adresse
  **/
  @JsonProperty("adresse")
  public Adresse getAdresse() {
    return adresse;
  }

  public void setAdresse(Adresse adresse) {
    this.adresse = adresse;
  }

  public Inscription adresse(Adresse adresse) {
    this.adresse = adresse;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Inscription {\n");
    
    sb.append("    etudiantId: ").append(toIndentedString(etudiantId)).append("\n");
    sb.append("    nom: ").append(toIndentedString(nom)).append("\n");
    sb.append("    prenom: ").append(toIndentedString(prenom)).append("\n");
    sb.append("    adresse: ").append(toIndentedString(adresse)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

