package org.universite.model;

import javax.validation.constraints.*;

import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Adresse  {
  
  @ApiModelProperty(value = "")
  private Integer numeroVoie = null;


@XmlType(name="TypeVoieEnum")
@XmlEnum(String.class)
public enum TypeVoieEnum {

@XmlEnumValue("RUE") RUE(String.valueOf("RUE")), @XmlEnumValue("IMPASSE") IMPASSE(String.valueOf("IMPASSE")), @XmlEnumValue("BOULEVARD") BOULEVARD(String.valueOf("BOULEVARD")), @XmlEnumValue("AVENUE") AVENUE(String.valueOf("AVENUE"));


    private String value;

    TypeVoieEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static TypeVoieEnum fromValue(String v) {
        for (TypeVoieEnum b : TypeVoieEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  @ApiModelProperty(value = "")
  private TypeVoieEnum typeVoie = null;

  @ApiModelProperty(value = "")
  private String nomVoie = null;

  @ApiModelProperty(required = true, value = "")
  private String codePostal = null;

  @ApiModelProperty(required = true, value = "")
  private String commune = null;
 /**
   * Get numeroVoie
   * @return numeroVoie
  **/
  @JsonProperty("numeroVoie")
  public Integer getNumeroVoie() {
    return numeroVoie;
  }

  public void setNumeroVoie(Integer numeroVoie) {
    this.numeroVoie = numeroVoie;
  }

  public Adresse numeroVoie(Integer numeroVoie) {
    this.numeroVoie = numeroVoie;
    return this;
  }

 /**
   * Get typeVoie
   * @return typeVoie
  **/
  @JsonProperty("typeVoie")
  public String getTypeVoie() {
    if (typeVoie == null) {
      return null;
    }
    return typeVoie.value();
  }

  public void setTypeVoie(TypeVoieEnum typeVoie) {
    this.typeVoie = typeVoie;
  }

  public Adresse typeVoie(TypeVoieEnum typeVoie) {
    this.typeVoie = typeVoie;
    return this;
  }

 /**
   * Get nomVoie
   * @return nomVoie
  **/
  @JsonProperty("nomVoie")
  public String getNomVoie() {
    return nomVoie;
  }

  public void setNomVoie(String nomVoie) {
    this.nomVoie = nomVoie;
  }

  public Adresse nomVoie(String nomVoie) {
    this.nomVoie = nomVoie;
    return this;
  }

 /**
   * Get codePostal
   * @return codePostal
  **/
  @JsonProperty("codePostal")
  @NotNull
 @Pattern(regexp="[0-9]{5}")  public String getCodePostal() {
    return codePostal;
  }

  public void setCodePostal(String codePostal) {
    this.codePostal = codePostal;
  }

  public Adresse codePostal(String codePostal) {
    this.codePostal = codePostal;
    return this;
  }

 /**
   * Get commune
   * @return commune
  **/
  @JsonProperty("commune")
  @NotNull
  public String getCommune() {
    return commune;
  }

  public void setCommune(String commune) {
    this.commune = commune;
  }

  public Adresse commune(String commune) {
    this.commune = commune;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Adresse {\n");
    
    sb.append("    numeroVoie: ").append(toIndentedString(numeroVoie)).append("\n");
    sb.append("    typeVoie: ").append(toIndentedString(typeVoie)).append("\n");
    sb.append("    nomVoie: ").append(toIndentedString(nomVoie)).append("\n");
    sb.append("    codePostal: ").append(toIndentedString(codePostal)).append("\n");
    sb.append("    commune: ").append(toIndentedString(commune)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

